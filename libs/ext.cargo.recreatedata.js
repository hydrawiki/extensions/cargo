/**
 * @author Yaron Koren
 */

( function( $, mw, cargo ) {
	'use strict';

	/**
	 * Class constructor
	 */
	cargo.recreateData = function() {};

	var recreateData = new cargo.recreateData();

	var cargoScriptPath = mw.config.get( 'cargoScriptPath' );
	var tableName = mw.config.get( 'cargoTableName' );
	var isDeclared = mw.config.get( 'cargoIsDeclared' );
	var viewTableURL = mw.config.get( 'cargoViewTableUrl' );
	var createReplacement = false;
	var templateData = mw.config.get( 'cargoTemplateData' );

	var numTotalPages = 0;
	var numTotalPagesHandled = 0;

	for ( var i = 0; i < templateData.length; i++ ) {
		numTotalPages += parseInt( templateData[i].numPages );
	}

	recreateData.replaceForm = function() {
		$("#recreateDataCanvas").html( "<div id=\"recreateTableProgress\"></div>" );
		$("#recreateDataCanvas").append( "<div id=\"recreateDataProgress\"></div>" );
	}

	/**
	 * Recursive function that uses Ajax to populate a Cargo DB table with
	 * the data for one or more templates.
	 */
	recreateData.createJobs = function( templateNum, numPagesHandled, replaceOldRows ) {
		var curTemplate = templateData[templateNum];
		var progressImage = "<img src=\"" + cargoScriptPath + "/skins/loading.gif\" />";
		if ( numTotalPages > 1000 ) {
			var remainingPixels = 100 * numTotalPagesHandled / numTotalPages;
			progressImage = "<progress value=\"" + remainingPixels + "\" max=\"100\"></progress>";
		}
		$("#recreateDataProgress").html( "<p>" + progressImage + "</p>" );
		var api = new mw.Api();
		api.postWithToken(
			'csrf',
			{
				action: "cargorecreatedata",
				table: tableName,
				template: curTemplate.name,
				offset: numPagesHandled,
				formatversion: 2,
				replaceOldRows: replaceOldRows
			}
		)
		.done(function( msg ) {
			var newNumPagesHandled = Math.min( numPagesHandled + 500, curTemplate.numPages );
			numTotalPagesHandled += newNumPagesHandled - numPagesHandled;
			if ( newNumPagesHandled < curTemplate.numPages ) {
				recreateData.createJobs( templateNum, newNumPagesHandled, replaceOldRows );
			} else {
				if ( templateNum + 1 < templateData.length ) {
					recreateData.createJobs( templateNum + 1, 0, replaceOldRows );
				} else {
					// We're done.
					if ( createReplacement ) {
						viewTableURL += "?_replacement";
					}
					var linkMsg = createReplacement ? 'cargo-cargotables-viewreplacementlink' : 'cargo-cargotables-viewtablelink';
					$("#recreateDataProgress").html( "<p>" + mw.msg( 'cargo-recreatedata-success' ) + "</p><p><a href=\"" + viewTableURL + "\">" + mw.msg( linkMsg ) + "</a>.</p>" );
				}
			}
		});
	}

	jQuery( "#cargoSubmit" ).click( function() {
		createReplacement = $("[name=createReplacement]").is( ":checked" );

		recreateData.replaceForm();

		if ( isDeclared ) {
			$("#recreateTableProgress").html( "<img src=\"" + cargoScriptPath + "/skins/loading.gif\" />" );
			var api = new mw.Api();
			api.postWithToken(
				'csrf',
				{
					action: "cargorecreatetables",
					template: templateData[0].name,
					formatversion: 2,
					createReplacement: createReplacement
				}
			)
			.done(function( msg ) {
				var displayMsg = createReplacement ? 'cargo-recreatedata-replacementcreated' : 'cargo-recreatedata-tablecreated';
				$("#recreateTableProgress").html( "<p>" + mw.msg( displayMsg, tableName ) + "</p>" );
				recreateData.createJobs( 0, 0, false );
			});
		} else {
			recreateData.createJobs( 0, 0, true );
		}
	});

	// This is not really needed at the moment, since no other JS code
	// is calling this code.
	recreateData.prototype = recreateData;

} )( jQuery, mediaWiki, cargo );